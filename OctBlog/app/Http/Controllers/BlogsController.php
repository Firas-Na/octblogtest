<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Jobs\SendingEmailsJob;
use App\User;
use Illuminate\Http\Request;


class BlogsController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Blog::with("comments")->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'type' => 'required',
            'details' => 'required'
        ]);

        $blog = Blog::create($request->all());
        $details = array();
        $users = User::all();
        foreach ($users as $u) {
            $details['emails'][] = ($u->email);
        }
        $details['body'] = "Hello, a new article has been created....";
        dispatch(new SendingEmailsJob($details));
        return $blog;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($blog)
    {
        $blog = Blog::with("comments")->find($blog);
        if ($blog->type == 'video') {
            return $blog->details;
        }
        return $blog;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return Blog::find($id)->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Blog::find($id)->delete();
    }
}
