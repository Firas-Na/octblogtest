<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $credentials = $request->validate([
            'email' => 'required|string',
            'password' => 'required|string'
        ]);

        if (!Auth::attempt($credentials)) {
            return response(['message' => 'invalid login credentials']);
        }

        $accessToken = Auth::user()->createToken('authToken')->accessToken; //create token takes the name of the token
        return response(['user' => Auth::user(), 'accessToken' => $accessToken]);
    }
}
