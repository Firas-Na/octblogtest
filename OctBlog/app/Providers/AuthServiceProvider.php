<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Passport::routes();

        //define the api scopes as: name:description
        Passport::tokensCan([
            'view-blog' => 'Clients who are allowed to view all blog posts on the server',
            'view-users' => 'Clients who are allowed to view all users on the server'
        ]);

        //defining time for each token to be expired
        Passport::tokensExpireIn(now()->addSeconds(120));
    }
}
