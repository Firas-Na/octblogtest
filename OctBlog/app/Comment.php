<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ["body", "blog_id"];


    public function blog()
    {
        return $this->belongsTo('App\Blog');
    }
}
