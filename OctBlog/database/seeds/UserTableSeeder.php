<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'=>'firas',
            'email'=>'asfoosh2014@gmail.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('firas_10')
        ]);

        User::create([
            'name'=>'maya',
            'email'=>'maya@gmail.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('maya_10')
        ]);

    }
}
