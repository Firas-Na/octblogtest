<?php

use Illuminate\Database\Seeder;
use App\Article;

class ArticleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Article::create([
            'title'=>'first Article',
            'body'=>'Creating a Restful API Test',
            'user_id'=>1
        ]);

        Article::create([
            'title'=>'second Article',
            'user_id'=>2
        ]);


        Article::create([
            'title'=>'third Article',
            'user_id'=>1
        ]);

        Article::create([
            'title'=>'fourth Article',
            'user_id'=>1
        ]);
    }
}
