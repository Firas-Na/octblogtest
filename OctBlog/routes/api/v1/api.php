<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['auth:api', 'scopes:view-blog'])->group(function () {

    //for scopes this should work but because i am on localhost and need a host for server and host for client
    Route::apiResource("/blog", 'blogsController');
    //Route::apiResource("/blog", 'BlogsController');
    Route::post("/blog/{blogid}/comment", 'CommentsController@addComment');
});

//User
Route::prefix('/user')->group(function () {
    Route::post('/login', 'api\v1\LoginController@login');

    Route::middleware('auth:api')->get('/me', 'api\v1\user\UserController@me');
    //to add multiple scopes:first-scope, second-scope, .....
    Route::middleware(['auth:api', 'scope:view-users'])->get('/all', 'api\v1\user\UserController@index');
});
