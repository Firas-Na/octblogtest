# OctBlogTest



## Getting started

Blog Management.

## Project Description

- OctBlog folder: contains the project's code (Laravel).
- OctCollection.postman_collection.json file: is the post collection which contains test api calls.

## Instructions

- First: need to modify `.env` file's variables:
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
MAIL_FROM_ADDRESS=null
so that it includes a valid mailtrap account so it can sends emails to all signed users when a new article is created.

- Second: i have used sqlite for simplicity.

- Third: please run the following commands:
`php artisan migrate:fresh`
`php artisan optimize`
`php artisan passport:install`

- Fourth: please run the following command to activate the queue jobs:
`php artisan queue:listen`

-Fifth: please go to `localhost:8000` and register a user.

- Sixth: please create `Outh personal access` token and set it in the postman's collection as a `bearer token` for the whole collection.


## Code Description

- i have used laravel passport package and created the following models:

1) Blog: each user can have many blogs.
2) Comment: each comment belongs to one blog.

- Created one view which is `developer.index.blade.php` which lists:
1) Oauth Clients
2) Authorized Clients
3) Personal Access Tokens

- Created 2 scopes:
1) view-users
2) view-blog

- Created one job:
`SendingEmailsJob` for sending emails in the background.

- Created 3 controllers:
1) DevelopersController: for redirecting authenticated users to the `developer.index.blade.php` view.
2) CommentsController: currently contains only one action `addComment` which is used to add a comment to an existing blog(article).
3) BlogsController: an api resource controller for blogs management (crud)

## Test


- run the command:
php artisan serve

- Login to the test user account.

- go to:
localhost:8000/developer

- you will see the available OAuth Clients and Personal Access Tokens (can manipulate them)

- now test the apis found in the postman collection `OctCollection.postman_collection.json`


## License
For open source projects, say how it is licensed.
